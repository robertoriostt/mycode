#!/usr/bin/python3
"""yaml.dumps() expects a simgle agument performs the YAML transformation,
   and returns that as a YAML string | Alta3 Research"""

# YAML is NOT part of the standard library
# python3 -m pip install pyyaml
import yaml

def main():
    """runtime code"""

    ## reate a blob of data to work with
    hitchhikers = [{"name": "Zamphod Beelebrox", "species": "betelgeusian"}, {"name": "Arthur Dent", "species": "Human"}]

    ## display our Python data (a list containing two directionaries)
    print(hitchhikers)

    ## Create the YAML string
    yamlstring = yaml.dump(hitchhikers)

    ## Display a single string of YAML
    print(yamlstring)

if __name__ == "__main__":
    main()
